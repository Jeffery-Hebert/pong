import pygame

pygame.font.init()

WIDTH, HEIGHT = 1920, 1200

FPS = 60

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

PADDLE_WIDTH, PADDLE_HEIGHT = 20, 100

BALL_RADIUS = 10

SCORE_FONT = pygame.font.SysFont("comicsans", 50)

WINNING_SCORE = 5

WIN = pygame.display.set_mode((WIDTH, HEIGHT))

pygame.display.set_caption("Pong")

class GameInformation:
    def __init__(self, left_hits, right_hits, left_score, right_score):
        self.left_hits = self.original_left_hits = left_hits
        self.left_score = self.original_left_score = left_score
        self.right_hits = self.original_right_hits = right_hits
        self.right_score = self.original_right_score = right_score

    def reset(self):
        self.left_hits = self.original_left_hits
        self.right_hits = self.original_right_hits
        self.left_score = self.original_left_score
        self.right_score = self.original_right_score



class Paddle:
    COLOR = WHITE
    VELOCITY = 8

    def __init__(self, x, y, width, height):
        self.x = self.original_x = x
        self.y = self.original_y = y
        self.width = width
        self.height = height

    def draw(self, win):
        pygame.draw.rect(win, self.COLOR, (self.x, self.y, self.width, self.height))

    def move(self, up=True):
        if up:
            self.y -= self.VELOCITY
        else:
            self.y += self.VELOCITY

    def reset(self):
        self.x = self.original_x
        self.y = self.original_y


class Ball:
    MAX_VELOCITY = 15
    COLOR = WHITE

    def __init__(self, x, y, radius):
        self.x = self.original_x = x
        self.y = self.original_y = y
        self.radius = radius
        self.x_VELOCITY = self.MAX_VELOCITY
        self.y_VELOCITY = 0

    def draw(self, win):
        pygame.draw.circle(win, self.COLOR, (self.x, self.y), self.radius)

    def move(self):
        self.x += self.x_VELOCITY
        self.y -= self.y_VELOCITY

    def reset(self):
        self.x = self.original_x
        self.y = self.original_y
        self.y_VELOCITY = 0
        self.x_VELOCITY *= -1


def draw(win, paddles, ball, left_score, right_score):
    win.fill(BLACK)

    left_score_text = SCORE_FONT.render(f"{left_score}", 1, WHITE)
    right_score_text = SCORE_FONT.render(f"{right_score}", 1, WHITE)
    win.blit(left_score_text, (WIDTH // 4 - left_score_text.get_width() // 2, 20))
    win.blit(
        right_score_text, (WIDTH * (3 / 4) - right_score_text.get_width() // 2, 20)
    )

    for paddle in paddles:
        paddle.draw(win)

    for i in range(10, HEIGHT, 2 * (HEIGHT // 20)):
        if i % 2 == 1:
            continue
        pygame.draw.rect(win, WHITE, (WIDTH // 2 - 5, i, 10, HEIGHT // 20))

    ball.draw(win)
    pygame.display.update()


def handle_collision(ball, left_paddle, right_paddle, info):
    if ball.y + ball.radius >= HEIGHT:
        ball.y_VELOCITY *= -1
    elif ball.y - ball.radius <= 0:
        ball.y_VELOCITY *= -1

    if ball.x_VELOCITY < 0:
        if ball.y >= left_paddle.y and ball.y <= left_paddle.y + left_paddle.height:
            if ball.x - ball.radius <= left_paddle.x + left_paddle.width:
                ball.x_VELOCITY *= -1

                middle_y = left_paddle.y + left_paddle.height / 2
                difference_in_y = middle_y - ball.y
                reduction_factor = (left_paddle.height / 2) / ball.MAX_VELOCITY
                y_velocity = difference_in_y / reduction_factor
                ball.y_VELOCITY = -1 * y_velocity
                info.left_hits += 1

    else:
        if ball.y >= right_paddle.y and ball.y <= right_paddle.y + right_paddle.height:
            if ball.x + ball.radius >= right_paddle.x:
                ball.x_VELOCITY *= -1

                middle_y = right_paddle.y + right_paddle.height / 2
                difference_in_y = middle_y - ball.y
                reduction_factor = (right_paddle.height / 2) / ball.MAX_VELOCITY
                y_velocity = difference_in_y / reduction_factor
                ball.y_VELOCITY = -1 * y_velocity
                info.right_hits += 1


def handle_paddle_movement(keys, left_paddle, right_paddle):
    if keys[pygame.K_w] and left_paddle.y - left_paddle.VELOCITY >= 0:
        left_paddle.move(up=True)
    if (
        keys[pygame.K_s]
        and left_paddle.y + left_paddle.VELOCITY + left_paddle.height <= HEIGHT
    ):
        left_paddle.move(up=False)
    if keys[pygame.K_UP] and right_paddle.y - right_paddle.VELOCITY >= 0:
        right_paddle.move(up=True)
    if (
        keys[pygame.K_DOWN]
        and right_paddle.y + right_paddle.VELOCITY + right_paddle.height <= HEIGHT
    ):
        right_paddle.move(up=False)


def main():
    run = True
    clock = pygame.time.Clock()

    left_paddle = Paddle(
        10, HEIGHT // 2 - PADDLE_HEIGHT // 2, PADDLE_WIDTH, PADDLE_HEIGHT
    )
    right_paddle = Paddle(
        WIDTH - 10 - PADDLE_WIDTH,
        HEIGHT // 2 - PADDLE_HEIGHT // 2,
        PADDLE_WIDTH,
        PADDLE_HEIGHT,
    )

    ball = Ball(WIDTH // 2, HEIGHT // 2, BALL_RADIUS)

    info = GameInformation(0,0,0,0)

    left_score = info.left_score
    right_score = info.right_score

    while run:
        clock.tick(FPS)
        draw(WIN, [left_paddle, right_paddle], ball, left_score, right_score)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                break

        keys = pygame.key.get_pressed()
        handle_paddle_movement(keys, left_paddle, right_paddle)

        ball.move()
        handle_collision(ball, left_paddle, right_paddle, info)
        if ball.x < 0:
            right_score += 1
            ball.reset()
            left_paddle.reset()
            right_paddle.reset()
            info.reset()
        elif ball.x > WIDTH:
            left_score += 1
            ball.reset()
            left_paddle.reset()
            right_paddle.reset()
            info.reset()

        won = False

        if right_score == WINNING_SCORE:
            won = True
            win_text = "Right Player Won!"
        if left_score == WINNING_SCORE:
            won = True
            win_text = "Right Player Won!"
        if won:
            text = SCORE_FONT.render(win_text, 1, WHITE)
            WIN.blit(
                text,
                (
                    WIDTH // 2 - text.get_width() // 2,
                    HEIGHT // 2 - text.get_height() // 2,
                ),
            )
            pygame.display.update()
            pygame.time.delay(5000)
            left_score = 0
            right_score = 0
    pygame.quit()


if __name__ == "__main__":
    main()
